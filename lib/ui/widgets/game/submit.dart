import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:tetrisdual/cubit/activity/activity_cubit.dart';
import 'package:tetrisdual/models/activity/player.dart';

class SubmitWidget extends StatelessWidget {
  const SubmitWidget({super.key, required this.player});

  final Player player;

  @override
  Widget build(BuildContext context) {
    const double gainFontSize = 70;

    const gainTestStyle = TextStyle(
      fontFamily: 'Blocks',
      fontSize: gainFontSize,
      fontWeight: FontWeight.bold,
    );
    const submitIcon = Icon(
      Icons.done_all,
      color: Colors.orange,
      size: gainFontSize / 2,
    );

    const topBorderBlack = BoxDecoration(
      border: Border(
        top: BorderSide(
          color: Colors.black,
          width: 2,
        ),
      ),
    );

    return Container(
      decoration: topBorderBlack,
      padding: const EdgeInsets.only(
        top: 10,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            '+${player.counter.computePoints()}',
            style: gainTestStyle,
            textHeightBehavior: const TextHeightBehavior(
              applyHeightToFirstAscent: false,
              applyHeightToLastDescent: false,
            ),
          ),
          const SizedBox(width: 10),
          IconButton(
            padding: EdgeInsets.zero,
            constraints: const BoxConstraints(),
            style: const ButtonStyle(
              tapTargetSize: MaterialTapTargetSize.shrinkWrap,
            ),
            icon: submitIcon,
            onPressed: () {
              player.score = player.score + player.counter.computePoints();
              player.counter.reset();

              BlocProvider.of<ActivityCubit>(context).toggleCurrentPlayer();
            },
          ),
          const SizedBox(width: 10),
        ],
      ),
    );
  }
}
