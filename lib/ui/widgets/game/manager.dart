import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:tetrisdual/cubit/activity/activity_cubit.dart';
import 'package:tetrisdual/models/activity/player.dart';
import 'package:tetrisdual/ui/widgets/game/counter.dart';
import 'package:tetrisdual/ui/widgets/game/submit.dart';

class ManagerWidget extends StatelessWidget {
  const ManagerWidget({super.key, required this.player});

  final Player player;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        return Expanded(
          child: Container(
            margin: const EdgeInsets.all(5),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: activityState.currentActivity.currentPlayer == player.playerId
                  ? [
                      CounterWidget(counter: player.counter),
                      SubmitWidget(player: player),
                    ]
                  : [],
            ),
          ),
        );
      },
    );
  }
}
