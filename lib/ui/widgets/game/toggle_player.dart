import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:tetrisdual/cubit/activity/activity_cubit.dart';

class TogglePlayerWidget extends StatelessWidget {
  const TogglePlayerWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapUp: (details) {
        BlocProvider.of<ActivityCubit>(context).toggleCurrentPlayer();
      },
      child: const Text(
        '🔄',
        style: TextStyle(
          fontSize: 50,
        ),
        textHeightBehavior: TextHeightBehavior(
          applyHeightToFirstAscent: false,
          applyHeightToLastDescent: false,
        ),
      ),
    );
  }
}
