import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:tetrisdual/cubit/activity/activity_cubit.dart';
import 'package:tetrisdual/models/activity/player.dart';
import 'package:tetrisdual/ui/widgets/game/board_painter.dart';

class TetriminoWidget extends StatelessWidget {
  const TetriminoWidget({super.key, required this.player});

  final Player player;

  @override
  Widget build(BuildContext context) {
    final ActivityCubit activityCubit = BlocProvider.of<ActivityCubit>(context);
    final double width = MediaQuery.of(context).size.width;

    return GestureDetector(
      onTapUp: (details) {
        if (player.playerId == activityCubit.getCurrentPlayer().playerId) {
          player.pickRandomTetrimino();
          activityCubit.refresh();
        }
      },
      child: CustomPaint(
        size: Size(width, width),
        willChange: false,
        painter: BoardPainter(player.currentTetrimino),
        isComplex: true,
        key: Key(player.currentTetrimino.toString()),
      ),
    );
  }
}
