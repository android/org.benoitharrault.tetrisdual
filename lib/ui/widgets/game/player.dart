import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:tetrisdual/cubit/activity/activity_cubit.dart';
import 'package:tetrisdual/models/activity/player.dart';
import 'package:tetrisdual/ui/widgets/game/manager.dart';
import 'package:tetrisdual/ui/widgets/game/tetrimino.dart';

class PlayerWidget extends StatelessWidget {
  const PlayerWidget({super.key, required this.player});

  final Player player;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ActivityCubit, ActivityState>(
      builder: (BuildContext context, ActivityState activityState) {
        final bool isActive = (activityState.currentActivity.currentPlayer == player.playerId);
        final double screenWidth = MediaQuery.of(context).size.width;
        final double tetriminoBlockWidth = screenWidth / 2.3;

        final Color borderColor = isActive ? Colors.greenAccent : Colors.blueGrey;
        const double borderWidth = 10;

        return Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(
              player.score.toString(),
              style: const TextStyle(
                fontFamily: 'Blocks',
                fontSize: 130,
                fontWeight: FontWeight.bold,
              ),
              textHeightBehavior: const TextHeightBehavior(
                applyHeightToFirstAscent: false,
                applyHeightToLastDescent: false,
              ),
            ),
            Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: borderColor,
                  width: borderWidth,
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox.square(
                    dimension: tetriminoBlockWidth,
                    child:
                        isActive ? TetriminoWidget(player: player) : const SizedBox.shrink(),
                  ),
                  ManagerWidget(player: player),
                ],
              ),
            )
          ],
        );
      },
    );
  }
}
