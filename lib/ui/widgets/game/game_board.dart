import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:tetrisdual/cubit/activity/activity_cubit.dart';
import 'package:tetrisdual/ui/widgets/game/player.dart';
import 'package:tetrisdual/ui/widgets/game/toggle_player.dart';

class GameBoardWidget extends StatelessWidget {
  const GameBoardWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final ActivityCubit activityCubit = BlocProvider.of<ActivityCubit>(context);

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          RotatedBox(
            quarterTurns: 2,
            child: PlayerWidget(
              player: activityCubit.getPlayer(1),
            ),
          ),
          const TogglePlayerWidget(),
          PlayerWidget(
            player: activityCubit.getPlayer(2),
          ),
        ],
      ),
    );
  }
}
