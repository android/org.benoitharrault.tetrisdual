import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:tetrisdual/cubit/activity/activity_cubit.dart';
import 'package:tetrisdual/models/activity/counter.dart';

class CounterWidget extends StatelessWidget {
  const CounterWidget({super.key, required this.counter});

  final Counter counter;

  static const double iconSize = 30.0;
  static const double fontSize = 50.0;
  static const double spacerHeight = 7;

  // Counter categories icons
  static const Color categoryIconColor = Colors.green;
  static const Icon iconTouchingColor = Icon(
    Icons.join_full,
    color: categoryIconColor,
    size: iconSize,
  );
  static const Icon iconRowsCount = Icon(
    Icons.table_rows,
    color: categoryIconColor,
    size: iconSize,
  );
  static const Icon iconHolesCount = Icon(
    Icons.check_box_outline_blank,
    color: categoryIconColor,
    size: iconSize,
  );

  // Action buttons icons
  static const Color buttonIconColor = Colors.blue;
  static const Icon iconRemove = Icon(
    Icons.remove,
    color: buttonIconColor,
    size: iconSize,
  );
  static const Icon iconAdd = Icon(
    Icons.add,
    color: buttonIconColor,
    size: iconSize,
  );

  @override
  Widget build(BuildContext context) {
    final ActivityCubit activityCubit = BlocProvider.of<ActivityCubit>(context);

    const spacer = TableRow(children: [
      SizedBox(height: spacerHeight),
      SizedBox(height: spacerHeight),
      SizedBox(height: spacerHeight),
      SizedBox(height: spacerHeight),
    ]);

    return Table(
      children: [
        buildTouchWidget(activityCubit),
        spacer,
        buildLinesWidget(activityCubit),
        spacer,
        buildHolesWidget(activityCubit),
        spacer,
      ],
    );
  }

  TableRow buildTouchWidget(ActivityCubit activityCubit) {
    return TableRow(
      children: [
        iconTouchingColor,
        IconButton(
          padding: EdgeInsets.zero,
          constraints: const BoxConstraints(),
          style: const ButtonStyle(
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          ),
          icon: iconRemove,
          onPressed: () {
            counter.touch = false;
            activityCubit.refresh();
          },
        ),
        Center(
          child: Icon(
            counter.touch ? Icons.radio_button_checked : Icons.radio_button_unchecked,
            color: categoryIconColor,
            size: iconSize,
          ),
        ),
        IconButton(
          padding: EdgeInsets.zero,
          constraints: const BoxConstraints(),
          style: const ButtonStyle(
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          ),
          icon: iconAdd,
          onPressed: () {
            counter.touch = true;
            activityCubit.refresh();
          },
        ),
      ],
    );
  }

  TableRow buildLinesWidget(ActivityCubit activityCubit) {
    return TableRow(
      children: [
        iconRowsCount,
        IconButton(
          padding: EdgeInsets.zero,
          constraints: const BoxConstraints(),
          style: const ButtonStyle(
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          ),
          icon: iconRemove,
          onPressed: () {
            counter.lines = max(counter.lines - 1, 0);
            activityCubit.refresh();
          },
        ),
        Center(
          child: Text(
            counter.lines.toString(),
            style: const TextStyle(
              fontFamily: 'Blocks',
              fontSize: fontSize,
            ),
            textHeightBehavior: const TextHeightBehavior(
              applyHeightToFirstAscent: false,
              applyHeightToLastDescent: false,
            ),
          ),
        ),
        IconButton(
          padding: EdgeInsets.zero,
          constraints: const BoxConstraints(),
          style: const ButtonStyle(
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          ),
          icon: iconAdd,
          onPressed: () {
            counter.lines = min(counter.lines + 1, 4);
            activityCubit.refresh();
          },
        ),
      ],
    );
  }

  TableRow buildHolesWidget(ActivityCubit activityCubit) {
    return TableRow(
      children: [
        iconHolesCount,
        IconButton(
          padding: EdgeInsets.zero,
          constraints: const BoxConstraints(),
          style: const ButtonStyle(
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          ),
          icon: iconRemove,
          onPressed: () {
            counter.holes = max(counter.holes - 1, 0);
            activityCubit.refresh();
          },
        ),
        Center(
          child: Text(
            counter.holes.toString(),
            style: const TextStyle(
              fontFamily: 'Blocks',
              fontSize: fontSize,
            ),
            textHeightBehavior: const TextHeightBehavior(
              applyHeightToFirstAscent: false,
              applyHeightToLastDescent: false,
            ),
          ),
        ),
        IconButton(
          padding: EdgeInsets.zero,
          constraints: const BoxConstraints(),
          style: const ButtonStyle(
            tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          ),
          icon: iconAdd,
          onPressed: () {
            counter.holes = min(counter.holes + 1, 9);
            activityCubit.refresh();
          },
        ),
      ],
    );
  }
}
