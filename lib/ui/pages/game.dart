import 'package:flutter/material.dart';

import 'package:tetrisdual/ui/widgets/game/game_board.dart';

class PageGame extends StatelessWidget {
  const PageGame({super.key});

  @override
  Widget build(BuildContext context) {
    return const GameBoardWidget();
  }
}
