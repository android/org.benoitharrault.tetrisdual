class Counter {
  // Current counter
  bool touch = false; // Does this new tetrimino touch an other tetrimino of same player
  int lines = 0; // Count lines fully filled by this new tetrimino
  int holes = 0; // Count non fillable holes caused by this new tetrimino

  // Points definitions
  static const int base = 50;
  static const int pointsIfTouch = 50;
  static const int pointsPerLine = 60;
  static const int pointsPerHole = -10;

  int computePoints() {
    return base + (touch ? pointsIfTouch : 0) + lines * pointsPerLine + holes * pointsPerHole;
  }

  void reset() {
    touch = false;
    lines = 0;
    holes = 0;
  }

  @override
  String toString() {
    return '$Counter(${toJson()})';
  }

  Map<String, dynamic> toJson() {
    return {
      'touch': touch,
      'lines': lines,
      'holes': holes,
    };
  }
}
