import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:tetrisdual/config/application_config.dart';
import 'package:tetrisdual/models/activity/player.dart';

class Activity {
  Activity({
    // Settings
    required this.activitySettings,

    // State
    this.isRunning = false,
    this.isStarted = false,
    this.isFinished = false,
    this.animationInProgress = false,

    // Base data
    this.players = const [],

    // Game data
    this.currentPlayer = 0,
  });

  // Settings
  final ActivitySettings activitySettings;

  // State
  bool isRunning;
  bool isStarted;
  bool isFinished;
  bool animationInProgress;

  // Base data
  List<Player> players;

  // Game data
  int currentPlayer;

  factory Activity.createEmpty() {
    return Activity(
      // Settings
      activitySettings: ActivitySettings.createDefault(appConfig: ApplicationConfig.config),
    );
  }

  factory Activity.createNew({
    ActivitySettings? activitySettings,
  }) {
    final ActivitySettings newActivitySettings = activitySettings ??
        ActivitySettings.createDefault(appConfig: ApplicationConfig.config);

    return Activity(
      // Settings
      activitySettings: newActivitySettings,
      // State
      isRunning: true,
      // Base data
      players: [
        Player(1),
        Player(2),
      ],
      // Game data
      currentPlayer: 0,
    );
  }

  bool get canBeResumed => isStarted && !isFinished;

  void dump() {
    printlog('');
    printlog('## Current game dump:');
    printlog('');
    printlog('$Activity:');
    printlog('  Settings');
    activitySettings.dump();
    printlog('  State');
    printlog('    isRunning: $isRunning');
    printlog('    isStarted: $isStarted');
    printlog('    isFinished: $isFinished');
    printlog('    animationInProgress: $animationInProgress');
    printlog('  Base data');
    printlog('    players: $players');
    printlog('  Game data');
    printlog('    currentPlayer: $currentPlayer');
    printlog('');
  }

  @override
  String toString() {
    return '$Activity(${toJson()})';
  }

  Map<String, dynamic>? toJson() {
    return <String, dynamic>{
      // Settings
      'activitySettings': activitySettings.toJson(),
      // State
      'isRunning': isRunning,
      'isStarted': isStarted,
      'isFinished': isFinished,
      'animationInProgress': animationInProgress,
      // Base data
      'players': players,
      // Game data
      'currentPlayer': currentPlayer,
    };
  }
}
