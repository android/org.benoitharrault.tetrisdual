import 'dart:math';

import 'package:tetrisdual/models/activity/counter.dart';

class Player {
  Player(this.playerId);
  int playerId;

  int score = 0;
  int currentTetrimino = 0;
  final Counter counter = Counter();

  void pickRandomTetrimino() {
    // ensure new tetrimino is not same as current one
    int newTetrimino = currentTetrimino;
    while (newTetrimino == currentTetrimino) {
      newTetrimino = Random().nextInt(5) + 1;
    }

    currentTetrimino = newTetrimino;
  }

  void resetTetrimino() {
    currentTetrimino = 0;
  }

  void submitPoints() {
    score = score + counter.computePoints();
    counter.reset();
  }

  @override
  String toString() {
    return '$Player(${toJson()})';
  }

  Map<String, dynamic> toJson() {
    return {
      'playerId': playerId,
      'score': score,
      'currentTetrimino': currentTetrimino,
      'counter': counter,
    };
  }
}
